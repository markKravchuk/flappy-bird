class MyBird {

    constructor(canvas, context) {
        this.frame = 0;
        this.animation = [
            {sX: 276, sY: 112},
            {sX: 276, sY: 139},
            {sX: 276, sY: 164},
            {sX: 276, sY: 139},
        ];
        this.w = 34;
        this.h = 26;
        this.x = 50;
        this.y = 150;
        // animation speed
        this.period = 10;
        // movement of bird
        this.speed = 0;
        this.gravity = 0.25;
        this.jump = 4.6;
        // for collision detection
        this.radius = 12;
        // rotate the bird
        this.rotation = 0;
        this.dead = false;
        this.distanceScore = 0;

        this.canvas = canvas;
        this.context = context;
    }


    draw(sprite) {
        let bird = this.animation[this.frame];

        this.context.save();
        this.context.translate(this.x, this.y);

        // rotating the canvas with rotation var
        this.context.rotate(this.rotation);

        // drawing on rotated canvas a bird
        this.context.drawImage(sprite, bird.sX, bird.sY, this.w, this.h, -this.w / 2,
            -this.h / 2, this.w, this.h);

        // restoring the canvas back
        this.context.restore();
    }

    // when user clicks on
    flap() {
        this.speed = -this.jump;
    }

    update() {

        if (state.current === state.getReady) {
            this.period = 10;
            this.y = 150;
            this.rotation = 0 * DEGREE;
        } else if (state.current === state.game || state.current === state.over) {
            this.speed += this.gravity;
            this.y += this.speed;


            // if bird in on fly
            if (this.dead === false) {
                // if not under bottom obstacle
                if (this.y + this.h / 2 >= this.canvas.height - fg.h) {
                    this.y = this.canvas.height - fg.h - this.h / 2;

                    //for AI purposes the death is avoided
                    //state.current = state.over;

                    this.die("fallen down");

                    //console.log("Too low");


                    this.speed = 0;
                    this.frame = 1;
                    //DIE.play();
                    return;
                }

                // if bird is on top screen

                if (this.y <= 0) {
                    this.die("too high");

                    //DIE.play();

                    this.speed = 0;
                    this.frame = 1;
                    return;
                }

                this.period = 5;

                this.distanceScore = frames;

            }

            // rotating the bird

            // if speed > jump -> bird is falling down
            if (this.speed >= this.jump) {
                this.rotation = 90 * DEGREE;
                this.frame = 1;
            } else {
                this.rotation = -25 * DEGREE;
            }
        }



        // if game is over, than flapping must not be
        if (this.dead === true) {
            return;
        }

        if (frames % this.period === 0) {
            this.frame++;
            if (this.frame === 4) {
                this.frame = 0;
            }
        }


    }

    inputs(pipes) {
        let inputs = [];

        // x: right corner
        // y: middle between 0 and (canvas.height(480) - fg.h(112))

        // default value if no pipes are
        let closestPipe = {
            x: this.canvas.width,
            y: (this.canvas.height - 112) / 2 - pipes.h
        };

        for (let i = 0; i < pipes.position.length; i++) {
            let pipe = pipes.position[i];

            if (!pipe.x + pipes.gap > this.x) {
                closestPipe.x = pipe.x;
                closestPipe.y = pipe.y;
                break;
            }
        }

        closestPipe.y += pipes.h;

        // position to top pipe
        inputs[0] = map(closestPipe.y, 0, this.canvas.height, 0, 1);

        // position to bottom pipe (top pipe + gap in between)
        inputs[1] = map((closestPipe.y + pipes.gap), pipes.gap, this.canvas.height, 0, 1);

        //bird`s y position
        inputs[2] = map(this.y, 0, this.canvas.height, 0, 1);

        //bird`s distance to the pipe
        let distance = closestPipe.x - this.x;
        inputs[3] = map(distance, 0, (this.canvas.width - this.x), 0, 1);

        return inputs;
    }

    die(cause) {
        console.log("Bird is dead: " + cause);
        this.dead = true;
    }

    reset() {
        this.speed = 0;
    }
}

function map(e, t, r, i, n, o) {
    let a = (e - t) / (r - t) * (n - i) + i;
    return o ? i < n ? this.constrain(a, i, n) : this.constrain(a, n, i) : a
}
