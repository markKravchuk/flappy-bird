const canvas = document.getElementById("bird");
const context = canvas.getContext("2d");

// NEAT configuration

let neat;
let TOTAL = 100;

let birds = [];

let config = {
    model: [
        {nodeCount: 4, type: "input"},
        {nodeCount: 2, type: "output", activationfunc: activation.SOFTMAX}
    ],
    mutationRate: 0.1,
    crossoverMethod: crossover.RANDOM,
    mutationMethod: mutate.RANDOM,
    populationSize: TOTAL
};

function preStart() {
    //initializing some variables before game

    state.current = state.game;

    neat = new NEAT(config);

    for (let i = 0; i < TOTAL; i++) {
        birds.push(new MyBird(canvas, context));
    }

    loop();
}

// GAME VARIABLES

let frames = 0;
const DEGREE = Math.PI / 180;

// LOAD SPRITE IMAGES
const sprite = new Image();
sprite.src = "../../img/sprite.png";

// GAME STATE
const state = {
    current: 0,
    getReady: 0,
    game: 1,
    over: 2
};

// loading background
const bg = {
    sX: 0,
    sY: 0,
    w: 275,
    h: 226,
    x: 0,
    y: canvas.height - 226,

    draw: function () {
        context.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x, this.y, this.w, this.h);
        context.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x + this.w, this.y, this.w, this.h);
    }
};

// loading a foreground
const fg = {
    sX: 276,
    sY: 0,
    w: 224,
    h: 112,
    x: 0,
    y: canvas.height - 112,
    dx: 2,
    draw: function () {
        context.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x, this.y, this.w, this.h);
        context.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x + this.w, this.y, this.w, this.h);
    },
    update: function () {
        if (state.current === state.game) {
            this.x = (this.x - this.dx) % (this.w / 2);
        }
    }
};

const aiInfo = {
    draw: function () {
        if(neat === undefined) return;

        let generation = "Gen: " + neat.generation;
        let total = "Total: " + TOTAL;
        let alive = 0;

        // calculate alive birds
        for (let i = 0; i < TOTAL; i++) {
            if(!birds[i].dead) {
                alive++;
            }
        }
        alive = "Alive: " + alive;

        let best = "Best: " + score.best;

        let y = canvas.height - fg.h + 45;

        context.font = "25px Teko";
        context.lineWidth = 1;

        context.fillText(generation, 20, y);
        context.strokeText(generation, 20, y);

        context.fillText(total, 20, y + 50);
        context.strokeText(total, 20, y + 50);

        context.fillText(best, canvas.width/2, y);
        context.strokeText(best, canvas.width/2, y);

        context.fillText(alive, canvas.width/2, y + 50);
        context.strokeText(alive, canvas.width/2, y + 50);
    }
};

// loading a bird

// loading a pipes
const pipes = {
    bottom: {
        sX: 502,
        sY: 0
    },
    top: {
        sX: 553,
        sY: 0
    },
    w: 53,
    h: 400,
    gap: 85,
    // speed of pipes moving
    dx: 2,
    //
    mapYPos: -150,
    position: [],

    update: function () {
        if (state.current !== state.game) return;
        if (frames % 100 === 0) {
            this.position.push({
                x: canvas.width,
                y: this.mapYPos * (Math.random() + 1),
                wasInside: false
            });
        }
        for (let i = 0; i < this.position.length; i++) {
            let p = this.position[i];

            // moving the pipes
            p.x -= this.dx;

            let bottomPipeY = p.y + this.h + this.gap;

            //if pipes gone beyond the canvas -> delete them
            if (p.x + this.w <= 0) {
                // remove it
                this.position.shift();
            }

            for (let j = 0; j < TOTAL; j++) {

                let oneBird = birds[j];

                // checking the collision between top pipe and bird
                if (oneBird.x + oneBird.radius > p.x && oneBird.x - oneBird.radius < p.x + this.w
                    && oneBird.y + oneBird.radius > p.y && oneBird.y - oneBird.radius < p.y + this.h) {
                    //console.log("Hit top pipe");

                    //state.current = state.over;
                    oneBird.die("hit top pipe");
                    continue;
                }

                // checking the collision between top pipe and bird
                if (oneBird.x + oneBird.radius > p.x && oneBird.x - oneBird.radius < p.x + this.w
                    && oneBird.y + oneBird.radius > bottomPipeY && oneBird.y - oneBird.radius < bottomPipeY + this.h) {
                    //console.log("Hit bottom pipe");
                    //state.current = state.over;
                    oneBird.die("hit bottom pipe");
                    continue;
                }


                if (oneBird.x + oneBird.radius > p.x + this.w / 4 && oneBird.x - oneBird.radius < p.x + this.w
                    && oneBird.y + oneBird.radius > p.x && oneBird.y - oneBird.radius > p.x + this.gap) {

                    if (!p.wasInside) {
                        console.log("Receive a reward!");
                        score.value++;
                        p.wasInside = true;
                        score.best = Math.max(score.value, score.best);
                    }
                }
            }
        }
    },
    draw: function () {
        for (let i = 0; i < this.position.length; i++) {
            let p = this.position[i];

            let topYPos = p.y;
            let bottomYPos = p.y + this.h + this.gap;

            // drawing top pipe
            context.drawImage(sprite, this.top.sX, this.top.sY, this.w, this.h, p.x, topYPos, this.w, this.h);

            // drawing bottom pipe according to top pipe + gap in between
            context.drawImage(sprite, this.bottom.sX, this.bottom.sY, this.w, this.h, p.x, bottomYPos, this.w, this.h);
        }
    },
    reset: function () {
        this.position = [];
    }
};

// creating score
const score = {
    best: 0,
    value: 0,
    draw: function () {
        context.fillStyle = "#FFF";
        context.strokeStyle = "#000";

        if (state.current === state.game) {
            context.lineWidth = 2;
            context.font = "35px Teko";
            context.fillText(this.value, canvas.width / 2, 50);
            context.strokeText(this.value, canvas.width / 2, 50);

        }
    },
    reset: function () {
        this.value = 0;
    }
};

// GET READY MESSAGE
const getReady = {
    sX: 0,
    sY: 228,
    w: 173,
    h: 152,
    x: canvas.width / 2 - 173 / 2,
    y: 80,
    draw: function () {

        if (state.current === state.getReady) {
            context.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x, this.y, this.w, this.h);
        }
    }
};

// GAME OVER MESSAGE
const gameOver = {
    sX: 175,
    sY: 228,
    w: 225,
    h: 200,
    x: canvas.width / 2 - 225 / 2,
    y: 90,
    draw: function () {
        if (state.current === state.over) {
            context.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x, this.y, this.w, this.h);
        }

    }
};

// UPDATE
function update() {
    fg.update();
    for (let i = 0; i < TOTAL; i++) {
        if (!birds[i].dead) birds[i].update();
    }
    pipes.update();

    //AI decisions

    for (let i = 0; i < TOTAL; i++) {
        neat.setInputs(birds[i].inputs(pipes), i);
    }

    neat.feedForward();

    let decisions = neat.getDesicions();
    for (let i = 0; i < TOTAL; i++) {
        if (decisions[i] === 1) {
            birds[i].flap();
        }
    }

    let finish = true;
    for (let z = 0; z < birds.length; z++) {
        if (!birds[z].dead) {
            finish = false;
            break;
        }
    }
    if (finish) {
        frames = 0;
        pipes.reset();
        console.log("Previous generation died, creating new");
        for (let i = 0; i < TOTAL; i++) {
            neat.setFitness(birds[i].distanceScore, i);
            birds[i] = new MyBird(canvas, context);
        }
        score.value = 0;
        neat.doGen();
    }
}

// DRAW
function draw() {
    // redrawing the canvas
    context.fillStyle = "#70c5ce";
    context.fillRect(0, 0, canvas.width, canvas.height);
    bg.draw();
    pipes.draw();
    fg.draw();

    birds.map(bird => {
        if(bird.dead === false) {
            bird.draw(sprite);
        }
    });

    getReady.draw();
    gameOver.draw();
    score.draw();
    aiInfo.draw();
}

// LOOP
function loop() {
    update();
    draw();
    frames++;
    requestAnimationFrame(loop);
}

state.current = state.game;
preStart();