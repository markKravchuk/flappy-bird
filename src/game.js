const canvas = document.getElementById("bird");
const context = canvas.getContext("2d");

// GAME VARIABLES

let frames = 0;
const DEGREE = Math.PI / 180;

// LOAD SPRITE IMAGES
const sprite = new Image();
sprite.src = "../img/sprite.png";

// GAME STATE
const state = {
    current: 0,
    getReady: 0,
    game: 1,
    over: 2
};

const SCORE_S = new Audio();
SCORE_S.src = "../audio/sfx_point.wav";

const FLAP = new Audio();
FLAP.src = "../audio/sfx_flap.wav";

const HIT = new Audio();
HIT.src = "../audio/sfx_hit.wav";

const SWOOSH = new Audio();
SWOOSH.src = "../audio/sfx_swooshing.wav";

const DIE = new Audio();
DIE.src = "../audio/sfx_die.wav";

// CONTROL THE GAME
document.addEventListener("click", function (e) {
    switch (state.current) {
        case state.getReady :
            state.current = state.game;
            SWOOSH.play();

            console.log("Info: speed: " + bird.speed)

            break;
        case state.game :
            bird.flap();
            FLAP.play();
            break;
        case state.over :
            // removing pipes from previous games

            let rect = canvas.getBoundingClientRect();

            let clickX = e.clientX - rect.left;
            let clickY = e.clientY - rect.top;

            if (clickX >= startBtn.x && clickX <= startBtn.x + startBtn.w
                && clickY >= startBtn.y && clickY <= startBtn.y + startBtn.h) {
                pipes.reset();
                score.reset();
                bird.reset();
                state.current = state.getReady;
            }
            break;
    }
});

// loading background
const bg = {
    sX: 0,
    sY: 0,
    w: 275,
    h: 226,
    x: 0,
    y: canvas.height - 226,

    draw: function () {
        context.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x, this.y, this.w, this.h);
        context.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x + this.w, this.y, this.w, this.h);
    }
};

// loading a foreground
const fg = {
    sX: 276,
    sY: 0,
    w: 224,
    h: 112,
    x: 0,
    y: canvas.height - 112,
    dx: 2,
    draw: function () {
        context.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x, this.y, this.w, this.h);
        context.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x + this.w, this.y, this.w, this.h);
    },
    update: function () {
        if (state.current === state.game) {
            this.x = (this.x - this.dx) % (this.w / 2);
        }
    }
};

// loading a bird
const bird = {
    frame: 0,
    animation: [
        {sX: 276, sY: 112},
        {sX: 276, sY: 139},
        {sX: 276, sY: 164},
        {sX: 276, sY: 139},
    ],
    w: 34,
    h: 26,
    x: 50,
    y: 150,
    // animation speed
    period: 10,
    // movement of bird
    speed: 0,
    gravity: 0.25,
    jump: 4.6,
    // for collision detection
    radius: 12,
    // rotate the bird
    rotation: 0,

    draw: function () {
        let bird = this.animation[this.frame];

        context.save();
        context.translate(this.x, this.y);

        // rotating the canvas with rotation var
        context.rotate(this.rotation);

        // drawing on rotated canvas a bird
        context.drawImage(sprite, bird.sX, bird.sY, this.w, this.h, -this.w / 2,
            -this.h / 2, this.w, this.h);

        // restoring the canvas back
        context.restore();
    },
    // when user clicks on
    flap: function () {
        this.speed = -this.jump
    },

    update: function () {

        if (state.current === state.getReady) {
            this.period = 10;
            this.y = 150;
            this.rotation = 0 * DEGREE;
        } else if (state.current === state.game || state.current === state.over) {
            this.speed += this.gravity;
            this.y += this.speed;

            // if bird in on fly
            if(state.current === state.game) {
                // if not under bottom obstacle
                if (this.y + this.h / 2 >= canvas.height - fg.h) {
                    this.y = canvas.height - fg.h - this.h / 2;
                    state.current = state.over;

                    console.log("Too low");

                    this.speed = 0;
                    this.frame = 1;
                    DIE.play();
                    return;
                }

                // if bird is on top screen

                if(this.y <= 0) {
                    console.log("Too high");

                    state.current = state.over;
                    DIE.play();

                    this.speed = 0;
                    this.frame = 1;
                    return;
                }

                this.period = 5;

                // rotating the bird
            }


            // if speed > jump -> bird is falling down
            if (this.speed >= this.jump) {
                this.rotation = 90 * DEGREE;
                this.frame = 1;
            } else {
                this.rotation = -25 * DEGREE;
            }
        }

        // if game is over, than flapping must not be
        if (state.current === state.over) {
            return;
        }

        if (frames % this.period === 0) {
            this.frame++;
            if (this.frame === 4) {
                this.frame = 0;
            }
        }


    },
    reset: function () {
        this.speed = 0;
    }

};

// loading a pipes
const pipes = {
    bottom: {
        sX: 502,
        sY: 0
    },
    top: {
        sX: 553,
        sY: 0
    },
    w: 53,
    h: 400,
    gap: 85,
    // speed of pipes moving
    dx: 2,
    //
    mapYPos: -150,
    position: [],

    update: function () {
        if (state.current !== state.game) return;
        if (frames % 100 === 0) {
            this.position.push({
                x: canvas.width,
                y: this.mapYPos * (Math.random() + 1),
                wasInside: false
            });
        }
        for (let i = 0; i < this.position.length; i++) {
            let p = this.position[i];

            // moving the pipes
            p.x -= this.dx;

            let bottomPipeY = p.y + this.h + this.gap;

            //if pipes gone beyond the canvas -> delete them
            if (p.x + this.w <= 0) {
                // remove it
                this.position.shift();
                break;
            }

            // checking the collision between top pipe and bird
            if (bird.x + bird.radius > p.x && bird.x - bird.radius < p.x + this.w
                && bird.y + bird.radius > p.y && bird.y - bird.radius < p.y + this.h) {
                console.log("Hit top pipe");
                state.current = state.over;
                HIT.play();
            }

            // checking the collision between top pipe and bird
            if (bird.x + bird.radius > p.x && bird.x - bird.radius < p.x + this.w
                && bird.y + bird.radius > bottomPipeY && bird.y - bird.radius < bottomPipeY + this.h) {
                console.log("Hit bottom pipe");
                HIT.play();
                state.current = state.over;
            }


            if (bird.x + bird.radius > p.x + this.w / 4 && bird.x - bird.radius < p.x + this.w
                && bird.y + bird.radius > p.x && bird.y - bird.radius > p.x + this.gap) {

                if (!p.wasInside) {
                    console.log("Receive a reward!");

                    SCORE_S.play();

                    score.value++;

                    p.wasInside = true;

                    score.best = Math.max(score.value, score.best);
                    localStorage.setItem("best", score.best);
                }
            }
        }
    },
    draw: function () {
        for (let i = 0; i < this.position.length; i++) {
            let p = this.position[i];

            let topYPos = p.y;
            let bottomYPos = p.y + this.h + this.gap;

            // drawing top pipe
            context.drawImage(sprite, this.top.sX, this.top.sY, this.w, this.h, p.x, topYPos, this.w, this.h);

            // drawing bottom pipe according to top pipe + gap in between
            context.drawImage(sprite, this.bottom.sX, this.bottom.sY, this.w, this.h, p.x, bottomYPos, this.w, this.h);
        }
    },
    reset: function () {
        this.position = [];
    }
};

// creating score
const score = {
    best: parseInt(localStorage.getItem("best")) || 0,
    value: 0,
    draw: function () {
        context.fillStyle = "#FFF";
        context.strokeStyle = "#000";

        if(state.current === state.game){
            context.lineWidth = 2;
            context.font = "35px Teko";
            context.fillText(this.value, canvas.width/2, 50);
            context.strokeText(this.value, canvas.width/2, 50);

        }else if(state.current === state.over){

            // SCORE VALUE
            context.font = "25px Teko";
            context.fillText(this.value, 225, 186);
            context.strokeText(this.value, 225, 186);

            // BEST SCORE
            context.fillText(this.best, 225, 228);
            context.strokeText(this.best, 225, 228);
        }
    },
    reset: function () {
        this.value = 0;
    }
};

// position of "start" button
const startBtn = {
    x: 120,
    y: 263,
    w: 83,
    h: 29
};

// GET READY MESSAGE
const getReady = {
    sX: 0,
    sY: 228,
    w: 173,
    h: 152,
    x: canvas.width / 2 - 173 / 2,
    y: 80,
    draw: function () {
        if (state.current === state.getReady) {
            context.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x, this.y, this.w, this.h);
        }
    }
};

// GAME OVER MESSAGE
const gameOver = {
    sX: 175,
    sY: 228,
    w: 225,
    h: 200,
    x: canvas.width / 2 - 225 / 2,
    y: 90,
    draw: function () {
        if (state.current === state.over) {
            context.drawImage(sprite, this.sX, this.sY, this.w, this.h, this.x, this.y, this.w, this.h);
        }

    }
};

// UPDATE
function update() {
    bird.update();
    fg.update();
    pipes.update();
}

// DRAW
function draw() {
    // redrawing the canvas
    context.fillStyle = "#70c5ce";
    context.fillRect(0, 0, canvas.width, canvas.height);
    bg.draw();
    pipes.draw();
    fg.draw();
    bird.draw();
    getReady.draw();
    gameOver.draw();
    score.draw();
}

// LOOP
function loop() {
    update();
    draw();
    frames++;
    requestAnimationFrame(loop);
}


loop();